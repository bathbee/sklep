<?php
/**
 * Single Product stock.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/stock.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function stockProduct(){
global $product;

$number = $product->get_stock_quantity();

if($number === 0){
	echo '<p class="braktowaru red_font">Dostępność: Brak towaru</p>';
} elseif($number > 0 && $number < 10){
	echo '<p class="nawyczerpaniu orange_font">Dostępność: Na wyczerpaniu</p>';
} elseif($number > 10 && $number < 20){
	echo '<p class="sredniailosc yellow_font">Dostępność: Średnia ilość</p>';
} elseif($number > 20){
	echo '<p class="duzailosc green_font">Dostępność: Duża ilość</p>';
}

}
stockProduct();
?>
<!-- <p class="stock <?php //echo esc_attr( $class ); ?>"> TEST <?php //echo wp_kses_post( $availability ); ?></p> -->