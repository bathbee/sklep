<?php
require 'systemPartnerski.php';

// add_action( 'wp_enqueue_scripts', 'parent_styles' );
// function parent_styles() {


//     wp_enqueue_style( 'dessau', get_template_directory_uri() . '/style.css' );
//     wp_enqueue_style( 'dessau-child',
//         get_stylesheet_directory_uri() . '/style.css',
//         array(  ),
//         wp_get_theme()->get('Version')
//     );
// }

add_filter('woocommerce_order_number', 'change_woocommerce_order_number');

function change_woocommerce_order_number($order_id)
{
    $prefix = date('Y') . '/' . date('m') . '/';
    $order_id = sprintf("%05d", $order_id);
    $new_order_id = $prefix . $order_id;
    // return $order_id;
    return $new_order_id;
}



add_action('wp', function () {
    if (isset($_GET['sP'])) {
    }
});


function checkAge($birthDate)
{
    $date = new DateTime($birthDate);
    $now = new DateTime();
    $interval = $now->diff($date);
    return $interval->y;
}

add_action('woocommerce_register_post', 'validate_birthday_register', 10, 3);
function validate_birthday_register($username, $email, $validation_errors)
{

    if (!isset($_POST['data_urodzenia']) || isset($_POST['data_urodzenia']) && empty($_POST['data_urodzenia'])) {
        $validation_errors->add('data_urodzenia_error', "Data urodzenia jest wymagana");
    }


    if (isset($_POST['data_urodzenia']) && !empty($_POST['data_urodzenia']) && checkAge(sanitize_text_field($_POST['data_urodzenia'])) <= 12) {
        $validation_errors->add('data_urodzenia_error', "Minimalny wiek wymagany do rejestracji to 12 lat.");
    }

    return $validation_errors;
}