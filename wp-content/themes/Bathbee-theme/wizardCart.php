<?php /* Template Name: Zamówienie */ get_header(); ?>
<div>

    <main role="main">
        <!-- section -->

        <div style="background:url(<?php echo get_template_directory_uri() ?>/img/bg-bathbee.png)"
            class="container-fluid bread-header-archive">
            <h1 class="d-block col-12 text-center"><?php the_title(); ?></h1>
        </div>
        <!-- <?php //echo do_shortcode(' [woocommerce_cart]'); 
                ?>
        <?php //echo do_shortcode(' [woocommerce_checkout] '); 
        ?> -->
        <section class="container" style="min-height:60vh;margin-top:30px">
            <?php
            if (!is_user_logged_in() && !is_wc_endpoint_url('order-received')) { ?>

            <link rel="stylesheet"
                href="<?php echo get_template_directory_uri(); ?>/wizardCart.css?ver=<?php WP_DEBUG ? time() : '1.0' ?>">

            <div class="wrapper">
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                        <header class="wizard__header">
                            <div class="wizard__header-overlay"></div>



                            <div class="wizard__steps">
                                <nav class="steps">
                                    <div class="step">
                                        <div class="step__content">
                                            <p class="step__number"><i class="fas fa-list"></i></p>
                                            <svg class="checkmark" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 52 52">
                                                <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                                <path class="checkmark__check" fill="none"
                                                    d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                            </svg>

                                            <div class="lines">
                                                <div class="line -start">
                                                </div>

                                                <div class="line -background">
                                                </div>

                                                <div class="line -progress">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="step">
                                        <div class="step__content">
                                            <p class="step__number"><i class="fas fa-sign-in-alt"></i> </p>
                                            <svg class="checkmark" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 52 52">
                                                <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                                <path class="checkmark__check" fill="none"
                                                    d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                            </svg>

                                            <div class="lines">
                                                <div class="line -background">
                                                </div>

                                                <div class="line -progress">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="step">
                                        <div class="step__content">
                                            <p class="step__number"><i class="fas fa-shopping-cart"></i></p>
                                            <svg class="checkmark" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 52 52">
                                                <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                                <path class="checkmark__check" fill="none"
                                                    d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                            </svg>

                                            <div class="lines">
                                                <div class="line -background">
                                                </div>

                                                <div class="line -progress">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </header>

                        <div class="panels">

                            <?php

                                global $woocommerce;
                                $deliveryCost = number_format((WC()->cart->get_shipping_total() * 77 / 100), 2, ',', '');
                                $amount = number_format(((WC()->cart->total - WC()->cart->get_shipping_total())  * 77 / 100), 2, ',', '');
                                $priceWithoutTax = preg_replace('#[^\d.]#', '', WC()->cart->get_total_ex_tax());

                                $lacznie = WC()->cart->total;
                                $vat = number_format(((WC()->cart->total) - (WC()->cart->total  * 77 / 100)), 2, ',', '');

                                ?>

                            <div class="panel">
                                <header class="panel__header">
                                </header>
                                <div class="row d-flex justify-content-center wrapper-summary-wizard">
                                    <div class="col-md-12 col-12 px-0 px-md-4">
                                        <?php echo do_shortcode(' [woocommerce_cart] '); ?>
                                    </div>
                                    <div class="col-md-3 col-12 summary-product-wizard d-none">
                                        <p class="netto">Wartość netto produktów: <span
                                                class="bold"><?php echo $amount; ?>
                                                zł</span></p>
                                        <p class="deliverycost">Wartość netto dostawy: <span
                                                class="bold"><?php echo $deliveryCost; ?>
                                                zł</span></p>
                                        <p class="vat">Podatek VAT łącznie: <span class="bold"><?php echo $vat; ?>
                                                zł</span>
                                        </p>
                                        <p class="brutto">Cena łącznie (brutto): <span
                                                class="bold"><?php echo  $lacznie; ?> zł</span></p>
                                    </div>

                                </div>
                            </div>

                            <div class="panel">
                                <header class="panel__header">
                                    <h2 class="panel__title">Jak zapłacić mniej?</h2>
                                    <?php require('howBuyCheap.php'); ?>
                                </header>

                                <p class="panel__content"></p>
                            </div>

                            <?php

                                $link = add_query_arg(array(
                                    'bathbee_cashback' => '1',
                                ), home_url() . "/zamowienie");
                                ?>

                            <form style="display:none" class="clickedFromPreferButton--form" method="post"
                                action="<?php echo home_url() ?>/moje-konto/">
                                <input type="hidden" name="clickedfromprefer" value="1" />
                            </form>

                            <div class="panel">
                                <header class="panel__header">
                                    <h2 class="panel__title">Bez rejestracji wiele tracisz!</h2>
                                    <p class="panel__subheading">Nasi zarejestrowani klienci wiele zyskują między innymi
                                        jako pierwsi dostają informacje o promocjach oraz oszczędzają na swoich
                                        kolejnych zakupach także w innych sklepach!</p>
                                    <p class="panel__subheading">Możesz się zarejestrować tutaj:</p>
                                    <?php

                                        echo '<a class="button green clickedFromPreferButton" href="' . $link . '">Zarejestruj się</a>';
                                        ?>
                                </header>
                            </div>




                        </div>

                        <div class="wizard__footer">
                            <button class="button previous">Wstecz</button>
                            <?php
                                echo '<a class="clickedFromPreferButton button green register-button" href="' . $link . '"><b>Kup w cenie preferencyjnej</b></a>';
                                ?>
                            <button class="button next">Kup w cenie regularnej</button>
                        </div>


                    </div>
                </div>

                <div class="wizard__congrats-message">
                    <?php echo do_shortcode(' [woocommerce_checkout] '); ?>
                </div>

                <?php
                    // $link = home_url('/zamowienie?bathbee_cashback=1#register'); echo '<a class="button button-prefer" href="'.$link.'">Kup w cenie preferencyjnej</a>';
                    ?>

                <script
                    src="<?php echo get_template_directory_uri(); ?>/js/wizardCart.js?ver=<?php WP_DEBUG ? time() : '1.0' ?>">
                </script>
                <?php } else {
                // echo do_shortcode('[bd_woocommerce_one_page_checkout]');
                echo do_shortcode(' [woocommerce_cart]');
                echo do_shortcode(' [woocommerce_checkout] ');
            } ?>
                <style>
                .woocommerce {
                    width: 100%
                }
                </style>
        </section>
    </main>
</div>
<?php get_footer(); ?>