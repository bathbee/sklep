// jQuery(function() {
//   var center = { lat: 51.9357358, lng: 16.8867698 };
//   var map = new google.maps.Map($(".map-canvas")[0], {
//     zoom: 7,
//     center: center
//   });

//   var offsetCenter = function(dx, dy) {
//     return { lat: dx, lng: dy };
//   };

//   var placements = [
//     {
//       type:
//         "GABINET KOSMETYCZNY BARBARA GŁĄB,<br> Kazimierza Wielkiego 65, 32-300 Olkusz",
//       LatLng: offsetCenter(50.2746842, 19.5750759)
//     },
//     {
//       type: "DOBRY PUNKT,<br> Harcerska 9, 41-400 Mysłowice",
//       LatLng: offsetCenter(50.2393034, 19.104306)
//     },
//     {
//       type: "STUDIO MASAŻU SMAR <br> LIDZBARK WARMIŃSKI, LIDZBARSKA 7/1",
//       LatLng: offsetCenter(53.6191665, 20.0636431)
//     },
//     {
//       type: "WARSZAWA BURAKOWSKA <br> WARSZAWA BURAKOWSKA 16/260",
//       LatLng: offsetCenter(52.2567614, 20.976623)
//     }
//   ];

//   $.each(placements, function(i, e) {
//     var marker = new google.maps.Marker({
//       map: map,
//       draggable: false,
//       position: e.LatLng
//     });
//     var info = new SnazzyInfoWindow(
//       $.extend(
//         {},
//         {
//           marker: marker,
//           placement: e.type,
//           content: e.type,
//           panOnOpen: false
//         }
//       )
//     );
//     // info.open();
//   });
// });
