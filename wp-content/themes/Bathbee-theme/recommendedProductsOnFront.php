<div class="recommendedProducts container">
    <p class="minimalHeading text-left">Polecane produkty</p>

    <div class="wrap-products">
        <?php

$args ='';
if(get_field('produkty_polecane_bottom', 'option')) {

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 4,
        'post__in'=> get_field('produkty_polecane_bottom', 'option')
      );
} else {

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 4,
        'meta_key' => 'total_sales',
        'orderby' => 'meta_value_num',
      );
}


  $loop = new WP_Query( $args );
  if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    wc_get_template_part( 'content', 'recommendProductsBottomFront' );
    endwhile;
  } else {
    echo __( 'No products found' );
  }
  wp_reset_postdata();
  ?>
    </div>
</div>