<div class="howbuycheap">
    <p class="heading-caveat">Zobacz jak kupić produkt taniej</p>
    <div class="step-how">
        <p class="stepnumber">Krok 1</p>
        <div class="wrapper-text-step">
            <p class="title-step">Zarejestruj się w <a href="<?php echo home_url('/zamowienie?bathbee_cashback=1#register'); ?>"> BathBee Cashback</a> </p>
            <p>Dzięki samej rejestracji zapłacisz aż o <b>10%</b> mniej za swoje zakupy! </p><p>Zdobywaj korzyści nie tylko w naszym sklepie, ale także w tysiącach innych na całym świecie</p>
        </div>
    </div>
    <div class="step-how">
        <p class="stepnumber">Krok 2</p>
        <div class="wrapper-text-step">
            <p class="title-step">Wprowadź Kod Rabatowy</p>
            <p> Wprowadź Kod Rabatowy, który możesz otrzymać od jednego z naszych <a
                    href="<?php echo get_permalink(6009) ?>">Partnerów</a> </p>

        </div>
    </div>
</div>