function clear_saveValue(e) {
  var id = e.id;
  var val = "";
  if (e.type === "radio") {
    localStorage.setItem("nowe_konto", "");
    localStorage.setItem("mam_konto", "");
    localStorage.setItem(id, val);
  } else {
    localStorage.setItem(id, val);
  }
}

function clear_getSavedValue(v) {
  if (!localStorage.getItem(v)) {
    return "";
  }
  return localStorage.getItem(v);
}

const clearForm = () => {
  const ids = [
    "reg_email",
    "billing_first_name",
    "billing_last_name",
    "billing_company",
    "billing_address_1",
    "billing_address_2",
    "billing_city",
    "billing_postcode",
    "billing_phone",
    "data_urodzenia",
    "numer_karty_cashback"
  ];

  ids.forEach(id => {
    const el = document.getElementById(id);
    if (el) {
      clear_saveValue(el);
      el.value = clear_getSavedValue(id);
    }
  });
};
clearForm();
alert("dziala");
