let openByID;

const offsetCenter = function(dx, dy) {
  return {
    lat: dx,
    lng: dy
  };
};

const renderMap = placements => {
  jQuery(function() {
    var center = {
      lat: 52,
      lng: 18.8867698
    };

    var center;
    if (window.innerWidth > 1000) {
      center = {
        lat: 52,
        lng: 14.6
      };
    } else {
      center = {
        lat: 55,
        lng: 18.8867698
      };
    }

    var mapStyle = [
      {
        featureType: "all",
        elementType: "labels.text",
        stylers: [{ visibility: "on" }]
      },
      {
        featureType: "all",
        elementType: "labels.text.fill",
        stylers: [
          { saturation: 36 },
          { color: "#333333" },
          { lightness: 40 },
          { visibility: "on" }
        ]
      },
      {
        featureType: "all",
        elementType: "labels.text.stroke",
        stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: 16 }]
      },
      {
        featureType: "administrative",
        elementType: "geometry.fill",
        stylers: [{ color: "#808080" }, { lightness: 20 }]
      },
      {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [{ color: "black" }, { lightness: 17 }, { weight: 1.2 }]
      },
      {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [{ color: "#f5f5f5" }, { lightness: 20 }]
      },
      {
        featureType: "poi",
        elementType: "geometry",
        stylers: [{ color: "#f5f5f5" }, { lightness: 21 }]
      },
      {
        featureType: "poi.park",
        elementType: "geometry",
        stylers: [{ color: "#dedede" }, { lightness: 21 }]
      },
      {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{ color: "#ffffff" }, { lightness: 17 }]
      },
      {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: 0.2 }]
      },
      {
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [{ color: "#ffffff" }, { lightness: 18 }]
      },
      {
        featureType: "road.local",
        elementType: "geometry",
        stylers: [{ color: "#ffffff" }, { lightness: 16 }]
      },
      {
        featureType: "transit",
        elementType: "geometry",
        stylers: [{ color: "#f2f2f2" }, { lightness: 19 }]
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [{ color: "#46BCEC" }, { lightness: 17 }]
      }
    ];

    let zoom = 6.3;
    if (window.innerWidth < 1000) {
      zoom = 5;
    }

    var map = new google.maps.Map($(".map-canvas")[0], {
      zoom: zoom,
      center: center,
      styles: mapStyle
    });

    openByID = id => {
      if (window.innerWidth < 1000 && id !== undefined) {
        $("body,html").animate(
          {
            scrollTop: 200
          },
          500
        );
      }
      for (let index = 0; index < placements.length; index++) {
        const e = placements[index];

        // placements.forEach(e => {
        var markerIcon = {
          path:
            "M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z",
          fillColor: "#84be38",
          fillOpacity: 0.95,
          scale: 1.5,
          strokeColor: "#fff",
          strokeWeight: 2,
          anchor: new google.maps.Point(12, 24)
        };

        var marker = new google.maps.Marker({
          map: map,
          draggable: false,
          position: e.LatLng,
          icon: markerIcon
        });
        var info = new SnazzyInfoWindow(
          $.extend(
            {},
            {
              marker: marker,
              placement: e.type,
              content: e.type,
              panOnOpen: false,
              closeWhenOthersOpen: true
            }
          )
        );
        if (index === id) info.open();
      }
    };

    openByID();
  });
};

// if (!window.points) {
// window.points = {
//   dolnośląskie: [
//     {
//       name: "Body4soul",
//       types: [],
//       identifier: null,
//       hours: [],
//       address: {
//         city: "Wrocław",
//         street: "ul. 3 Maja",
//         house: "100",
//         aparament: null
//       },
//       lat: "51.1271646",
//       len: "16.921653",
//       branch: [{name:'test'}],
//       phone: "123 456 789",
//       requireContact: null
//     },
//     {
//       name: "Punkt przykładowy",
//       types: [
//         "Punkt Odbioru Zamówień internetowych",
//         "Stacjonarna Sprzedaż Produktów"
//       ],
//       identifier: null,
//       hours: ["12:30", "16:00"],
//       address: {
//         city: "Wrocław",
//         street: "ul. 3 Maja",
//         house: "150",
//         aparament: null
//       },
//       lat: "51.9271646",
//       len: "16.021653",
//       branch: [{name:'test'}],
//       phone: "123 456 789",
//       requireContact: false
//     }
//   ],
//   małopolskie: [
//     {
//       name: "Punkt przykładowy kraków",
//       types: [
//         "Dystrybutor Sprzedaży Bezpośredniej",
//         "Punkt Akceptujący Karty Bathbee Cashback i Karty sieci Cashback World"
//       ],
//       identifier: null,
//       hours: ["12:30", "16:00"],
//       address: {
//         city: "Kraków",
//         street: "ul. 3 Maja",
//         house: "150",
//         aparament: null
//       },
//       lat: "50.0468547",
//       len: "19.9346621",
//       branch: [{name:'test'}],
//       phone: "123 456 789",
//       requireContact: false
//     }
//   ]
// };
// } else {
//   window.points = JSON.parse(window.points);
// }

// window.points = JSON.parse(window.points);

$(".dropdown-container")
  .on("click", ".dropdown-button", function() {
    if (
      $(this)
        .siblings(".dropdown-list")
        .is(":hidden")
    ) {
      $(".dropdown-button").removeClass("show");
      $(".dropdown-list").hide();
      $(this)
        .siblings(".dropdown-list")
        .show();
      $(this).addClass("show");
    } else if (
      $(this)
        .siblings(".dropdown-list")
        .is(":visible")
    ) {
      $(this)
        .siblings(".dropdown-list")
        .hide();
      $(this).removeClass("show");
    }
  })
  .on("input", ".dropdown-search", function() {
    var target = $(this);
    var dropdownList = target.closest(".dropdown-list");
    var search = target.val().toLowerCase();

    if (!search) {
      dropdownList.find("li").show();
      return false;
    }

    dropdownList.find("li").each(function() {
      var text = $(this)
        .text()
        .toLowerCase();
      var match = text.indexOf(search) > -1;
      $(this).toggle(match);
    });
  })
  .on("change", '[type="checkbox"]', function(e) {
    var container = $(this).closest(".dropdown-container");
    var numChecked = container.find('[type="checkbox"]:checked').length;
    container.find(".quantity").text(numChecked || "Wybierz");

    if (e.target.checked) {
      window.Filter[e.target.dataset.filter].push(e.target.name);
      window.filterBy.push(e.target.dataset.filter);
      window.filterBy = [...new Set(window.filterBy)];
    } else {
      window.Filter[e.target.dataset.filter].splice(
        window.Filter[e.target.dataset.filter].indexOf(e.target.name),
        1
      );
      if (window.Filter[e.target.dataset.filter].length === 0) {
        window.filterBy.splice(
          window.filterBy.indexOf(e.target.dataset.filter),
          1
        );
      }
    }

    const valuesFilters = [
      ...window.Filter.wojewodztwo,
      ...window.Filter.miasto,
      ...window.Filter.typ,
      ...window.Filter.branze
    ];

    filterData(
      placements(window.points).readyPoints,
      window.filterBy,
      valuesFilters
    );

    renderMap(
      filterData(
        placements(window.points).readyPoints,
        window.filterBy,
        valuesFilters
      )
    );
    renderResult(
      filterData(
        placements(window.points).readyPoints,
        window.filterBy,
        valuesFilters
      )
    );
  });

const placements = points => {
  const readyPoints = [];

  const filters = {
    wojewodztwa: [],
    miasta: [],
    typy: [],
    branze: []
  };

  let id = 0;
  for (const [key, value] of Object.entries(points)) {
    points[key].forEach(place => {
      filters.wojewodztwa.push(key);
      filters.miasta.push(place.address.city);

      let addTypes = [];
      place.types.forEach(type => {
        addTypes.push(type.name);
      });

      filters.typy = [...filters.typy, ...addTypes];

      let addBranches = [];
      place.branch.forEach(branch => {
        if (branch !== "b/d") {
          addBranches.push(branch);
        }
      });

      filters.branze = [...filters.branze, ...addBranches];

      let img = [];
      if (place.types.length > 0) {
        place.types.forEach(type => {
          if (type.name.includes("Wydawanie zamówień internetowych")) {
            img.push({
              link: window.RolesImg.punktOdbioruZamowieninternetowych,
              typ: "Wydawanie zamówień internetowych"
            });
          }
        });

        place.types.forEach(type => {
          if (type.name.includes("Stacjonarna Sprzedaż Produktów")) {
            img.push({
              link: window.RolesImg.stacjonarnaSprzedazProduktow,
              typ: "Stacjonarna Sprzedaż Produktów"
            });
          }
        });

        place.types.forEach(type => {
          if (type.name.includes("Dystrybutor Sprzedaży Bezpośredniej")) {
            img.push({
              link: window.RolesImg.dystrybutorSprzedazyBezposredniej,
              typ: "Dystrybutor Sprzedaży Bezpośredniej"
            });
          }
        });

        place.types.forEach(type => {
          if (type.name.includes("Mobilny Punkt Odbioru")) {
            img.push({
              link: window.RolesImg.mobilnyPunktOdbioru,
              typ: "Mobilny Punkt Odbioru"
            });
          }
        });

        place.types.forEach(type => {
          if (type.name.includes("Usługi na produktach Bathbee")) {
            img.push({
              link: window.RolesImg.usługiNaProduktachBathbee,
              typ: "Usługi na produktach Bathbee"
            });
          }
        });

        place.types.forEach(type => {
          if (
            type.name.includes(
              "Punkt Akceptujący Karty Bathbee Cashback i Karty sieci Cashback World"
            )
          ) {
            img.push({
              link: window.RolesImg.kartyBathbee,
              typ:
                "Punkt Akceptujący Karty Bathbee Cashback i Karty sieci Cashback World"
            });
          }
        });
      } else {
        img.push({
          link: window.RolesImg.stacjonarnaSprzedazProduktow,
          typ: "Stacjonarna Sprzedaż Produktów"
        });
      }

      const linkMap = () => {
        let html = "";
        if (place.lat && place.len)
          html += `https://www.google.com/maps/dir/@${Number(
            place.lat
          )},${Number(
            place.len
          )},12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x471644c0354e18d1:0xb46bb6b576478abf!2m2!1d19.9449799!2d50.0646501`;

        return html;
      };

      const images = () => {
        let html = "";
        img.forEach(i => {
          html += `<img class="ikona-punkt" title="${i.typ}" src="${i.link}"/>`;
        });
        return html;
      };

      const places = () => {
        let html = "";
        if (place.types.length > 0) {
          place.types.forEach(typ => {
            html += `<span title="${typ.name}" class="typ">${typ.name}</span>`;
          });
        } else {
          [{ name: "Stacjonarna Sprzedaż Produktów" }].forEach(typ => {
            html += `<span title="${typ.name}" class="typ">${typ.name}</span>`;
          });
        }

        return html;
      };

      const odbior = () => {
        let html = "";
        if (place.requireContact !== null) {
          if (place.requireContact) {
            html += `<p class="obior">Odbiór po wcześniejszym kontakcie telefonicznym.</p>`;
          } else if (place.hours) {
            html += `<p class="godziny"><span>Godzny otwarcia:</span> ${place.hours}</p>`;
          } else {
            html += `<p class="godziny"><span>Godzny otwarcia:</span> 11:00 - 13:00</p>`;
          }
        } else {
          if (place.hours) {
            html += `<p class="godziny"><span>Godzny otwarcia:</span> ${place.hours}</p>`;
          } else {
            html += `<p class="godziny"><span>Godzny otwarcia:</span> 11:00 - 13:00</p>`;
          }
        }

        return html;
      };

      const address = () => {
        let html = "";
        if (place.address.city) {
          html += `${place.address.city}, `;
        } else {
          html += "Przykładowe Miasto";
        }

        if (place.address.street) {
          html += `${place.address.street} `;
        }

        if (place.address.house) {
          html += `${place.address.house}`;
        }

        if (place.address.house && place.address.aparament) {
          html += `/`;
        }

        if (place.address.aparament) {
          html += `${place.address.aparament} `;
        }

        return html;
      };

      const branze = () => {
        let html = "";

        place.branch.forEach(branch => {
          if (branch !== "b/d") {
            html += `<span title="${branch}"class="branch">${branch}</span>`;
          } else {
            html += `<span title="Kosmetyczna" class="branch">Kosmetyczna</span>`;
          }
        });
        return html;
      };

      const placement = {
        type: `<div class="info-point"><div class="wrap-images-points">${images()}</div>  
        <p class="name"> <span class="nazwapunktu">${place.name}</span></p> 
        <p class="adress"><a title="Zobacz dojazd" target="_blank" href="${linkMap()}"><span>Adres:</span> ${address()}</a> </p>
        ${odbior()}
        <p><span>Tel.: </span><a href="tel:+48${place.phone}">${
          place.phone
        }</a></p>
        <p class="branches"><span>Branża:</span> ${branze()}</p>
        <p class="type"><span>Typ:</span> ${places()}</p>
    
        </div>`,
        LatLng:
          place.lat && place.len
            ? { lat: Number(place.lat), lng: Number(place.len) }
            : {
                lat: Number("51.14098802188077"),
                lng: Number("17.034054964980896")
              },
        typ:
          place.types.length > 0
            ? place.types
            : [{ name: "Stacjonarna Sprzedaż Produktów" }],
        wojewodztwo: key,
        miasto: place.address.city ? place.address.city : "Kraków",
        id: id,
        branze:
          place.branch && place.branch[0] !== "b/d"
            ? place.branch
            : ["Kosmetyczna", "Weselna"]
      };
      ++id;

      readyPoints.push(placement);
    });
  }

  filters.wojewodztwa = [...new Set(filters.wojewodztwa)];
  filters.miasta = [...new Set(filters.miasta)];
  filters.typy = [...new Set(filters.typy)];
  filters.branze = [...new Set(filters.branze)];

  return { readyPoints, filters };
};

const renderFilter_wojewodztwa = names => {
  let data = names;
  if (names.length === 0) {
    data = ["Małopolskie"];
  }
  data.forEach(name => {
    const stateTemplate = _.template(
      `<li> 
      <label><input data-filter="wojewodztwo" name="${name}" type="checkbox" class="filter">
      ${name}
        </label>
        </li>`
    );

    $(".wojewodztwa ul").append(stateTemplate());
  });
};

const renderFilter_miasta = names => {
  let data = names;
  if (names.length === 0) {
    data = ["Kraków"];
  }
  data.forEach(name => {
    const stateTemplate = _.template(
      `<li> 
      <label><input data-filter="miasto" name="${name}" type="checkbox" class="filter">
      ${name}
        </label>
        </li>`
    );

    $(".miasta ul").append(stateTemplate());
  });
};

const renderFilter_typy = names => {
  let data = names;
  if (names.length === 0) {
    data = ["Stacjonarna Sprzedaż Produktów"];
  }
  data.forEach(name => {
    const stateTemplate = _.template(
      `<li> 
      <label><input data-filter="typ" name="${name}" type="checkbox" class="filter">
      ${name}
        </label>
        </li>`
    );

    $(".typy ul").append(stateTemplate());
  });
};

const renderFilter_branze = names => {
  let data = names;
  if (names.length === 0) {
    data = ["Kosmetyczna", "Weselna"];
  }
  data.forEach(name => {
    const stateTemplate = _.template(
      `<li> 
      <label><input data-filter="branze" name="${name}" type="checkbox" class="filter">
      ${name}
        </label>
        </li>`
    );

    $(".branza ul").append(stateTemplate());
  });
};

const initStateFilter = () => {
  window.Filter = {
    wojewodztwo: [],
    miasto: [],
    typ: [],
    branze: []
  };
  window.filterBy = [];
};

const filterData = (data, keys, values) => {
  return data.filter(function(e) {
    return keys.every(function(a) {
      if (a !== "typ" && a !== "branze") {
        return values.includes(e[a]);
      } else if (a === "branze") {
        let info = false;
        e[a].forEach(v => {
          if (values.includes(v)) {
            info = true;
          }
        });

        return info;
      } else if (a === "typ") {
        let info = false;
        e[a].forEach(v => {
          if (values.includes(v.name)) {
            info = true;
          }
        });

        return info;
      }
    });
  });
};

//render result list poczatek

const renderResult = data => {
  let wojewodztwa = [];
  data.forEach(el => wojewodztwa.push(el.wojewodztwo));
  wojewodztwa = [...new Set(wojewodztwa)];

  resultsWrapper = document.querySelector(".results-wrapper");
  resultsWrapper.innerHTML = "";

  wojewodztwa.forEach(wojewodztwo => {
    const div = document.createElement("div"),
      p = document.createElement("p"),
      ul = document.createElement("ul");

    p.className = "wojewodztwo-nazwa";
    p.innerText = wojewodztwo;
    div.className = "col-12 p-0";
    div.appendChild(p);
    ul.className = "punkty-lista";

    const list = data.filter(elem => elem.wojewodztwo === wojewodztwo);

    list.forEach(place => {
      const div = document.createElement("div");
      div.className = "punkt-lista";
      div.innerHTML = place.type;

      const button = document.createElement("button");
      button.className = "zobacznamapie";
      button.setAttribute("onclick", `openByID(${place.id})`);
      button.innerText = "Zobacz na mapie";
      div.appendChild(button);

      ul.appendChild(div);
    });

    div.appendChild(ul);
    resultsWrapper.appendChild(div);
  });
};

//render result list koniec

const hideDropdown = () => {
  const dropDowns = document.querySelectorAll(".dropdown-button");
  const dropDownLists = document.querySelectorAll(".dropdown-list");
  window.addEventListener("click", e => {
    if (
      !e.target.className.includes("dropdown-button") &&
      !e.target.className.includes("dropdown-label") &&
      !e.target.className.includes("dropdown-quantity") &&
      !e.target.className.includes("quantity") &&
      !e.target.className.includes("filter") &&
      e.target.tagName !== "LABEL" &&
      e.target.tagName !== "INPUT"
    ) {
      dropDowns.forEach(dropDown => {
        dropDown.classList.remove("show");
      });
      dropDownLists.forEach(dropDownList => {
        dropDownList.style.display = "none";
      });
    }

    console.log(e.target.className, e.target.tagName);
  });
};

const punktyOdbioru = () => {
  jQuery(document).ready(() => {
    const points = window.points;
    const places = placements(points);
    initStateFilter();
    renderMap(places.readyPoints);
    renderFilter_wojewodztwa(places.filters.wojewodztwa);
    renderFilter_miasta(places.filters.miasta);
    renderFilter_typy(places.filters.typy);
    renderFilter_branze(places.filters.branze);
    renderResult(places.readyPoints);
    hideDropdown();
  });
};

punktyOdbioru();
