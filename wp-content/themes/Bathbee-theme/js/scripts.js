jQuery(document).ready(function() {
  if (window.innerWidth < 1000) {
    const menuChange = () => {
      const menu = document.getElementById("primary-menu");
      const koszyk = document.getElementById("koszyk");
      const loginRegisterLink = document.querySelector(
        ".featured-text .login a"
      );
      const li = document.createElement("li");
      const menuGlowne = document.getElementById("menu-menu-glowne");
      jQuery(li).append(loginRegisterLink);
      menuGlowne.appendChild(li);
      jQuery(koszyk).insertBefore(menu);
    };
    menuChange();
  }

  const infoPrefPrice = () => {
    const products = document.getElementsByClassName("productTwoPrices");
    [...products].forEach(product => {
      const preferencyjna = product.querySelector(".preferencyjna");

      if (preferencyjna) {
        const priceWrapper = product.querySelector(".price");
        const p = document.createElement("p");
        p.classList.add("info-pref-price");
        p.innerText =
          "*Cena preferencyjna tylko dla zarejestrowanych klientów z kuponem rabatowym";
        priceWrapper.parentNode.insertBefore(p, priceWrapper.nextSibling);

        const promo = product.querySelector("ins");
        if (promo) {
          const promo2 = product.querySelector("del");
          promo2.classList.add("wrapp-promotion");
          promo.classList.add("wrapp-promotion");

          jQuery(".wrapp-promotion").wrapAll(
            '<div class="wrapper-promotion"></div>'
          );
        }
      }
    });
  };
  infoPrefPrice();

  const infoPrefPriceSingleProduct = () => {
    const product = document.querySelector(".summary.entry-summary");
    if (product) {
      const preferencyjna = product.querySelector(".preferencyjna");

      if (preferencyjna) {
        const priceWrapper = product.querySelector(".price");
        const p = document.createElement("p");
        p.classList.add("info-pref-price-single-product");
        p.innerText =
          "*Cena preferencyjna tylko dla zarejestrowanych klientów z kuponem rabatowym";
        priceWrapper.parentNode.insertBefore(p, priceWrapper.nextSibling);
      }
    }
  };

  infoPrefPriceSingleProduct();
});

$(document).ready(function() {
  "use strict";

  var products = function() {
    var self = this;

    self.class = {
      item: "js-item",
      box: "js-box",
      close: "home-components__box__close"
    };

    self.dom = $(".home-components");

    self.el = {
      item: self.dom.find("." + self.class.item),
      box: self.dom.find("." + self.class.box),
      close: self.dom.find("." + self.class.close),
      modal: self.dom.find("." + self.class.modal),
      modal__close: self.dom.find("." + self.class.modal__close)
    };

    self.Init();
  };

  products.prototype.Init = function() {
    var self = this;

    self.el.item.on("click", function() {
      var that = $(this);

      self.el.item.removeClass("is-flipped");
      $(this).addClass("is-flipped");

      self.el.box.removeClass("active");
      if ($(window).width() < 767) {
        $("#box_custom36").css("z-index", "9999999999");
      }
      self.el.box.each(function() {
        if ($(this).data("type") == that.data("type")) {
          if ($(window).width() < 767) {
            $("body").addClass("no-scroll");
          }
          $(this).addClass("active");
        }
      });
    });

    self.el.close.on("click", function() {
      self.el.box.removeClass("active");
      if ($(window).width() < 767) {
        $("#box_custom36").css("z-index", "1");
        $("body").removeClass("no-scroll");
      }
    });
  };

  var Products = new products();
});

const setCookie = (name, value, time) => {
  var expires = "";
  if (time) {
    var date = new Date();
    date.setTime(date.getTime() + 1 * 1 * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
};

const getCookie = name => {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};

const loginRegisterSwitch = () => {
  const loginButton = document.querySelector('a[href="#tab_login"]');
  const registerButton = document.querySelector('a[href="#tab_register"]');

  const switchTabs = type => {
    if (type === "login") {
      loginButton.classList.add("active");
      registerButton.classList.remove("active");
      setCookie("myAccountTab", "login", 1);
    } else {
      loginButton.classList.remove("active");
      registerButton.classList.add("active");
      setCookie("myAccountTab", "register", 1);
    }
  };

  if (loginButton && registerButton) {
    loginButton.classList.add("active");
    loginButton.addEventListener("click", e => switchTabs("login"));
    registerButton.addEventListener("click", e => switchTabs("register"));
  }
};

const getAnchorLocation = () => {
  const url = window.location.href,
    idx = url.indexOf("#");
  const hash = idx != -1 ? url.substring(idx + 1) : "";
  return hash;
};

const switchTabsCookie = () => {
  const type = getCookie("myAccountTab");
  const loginButton = document.querySelector('a[href="#tab_login"]');
  const registerButton = document.querySelector('a[href="#tab_register"]');
  const tab_register = document.querySelector("#tab_register");
  const tab_login = document.querySelector("#tab_login");
  const tab = getAnchorLocation();

  if (loginButton && registerButton) {
    if (tab === "login") {
      loginButton.classList.add("active");
      registerButton.classList.remove("active");
      tab_login.classList.add("tab-active");
      tab_register.classList.remove("tab-active");
    } else if (tab === "register") {
      loginButton.classList.remove("active");
      registerButton.classList.add("active");
      tab_register.classList.add("tab-active");
      tab_login.classList.remove("tab-active");
    } else if (type === "register") {
      loginButton.classList.remove("active");
      registerButton.classList.add("active");
      tab_register.classList.add("tab-active");
      tab_login.classList.remove("tab-active");
    } else if (type === "login") {
      loginButton.classList.add("active");
      registerButton.classList.remove("active");
      tab_login.classList.add("tab-active");
      tab_register.classList.remove("tab-active");
    } else {
      loginButton.classList.add("active");
      registerButton.classList.remove("active");
      tab_login.classList.add("tab-active");
      tab_register.classList.remove("tab-active");
    }
  }
};

const backToOrderFromRegister = () => {
  const buttons = document.querySelectorAll(".clickedFromPreferButton");
  if (buttons.length > 0) {
    buttons.forEach(button => {
      button.addEventListener("click", e => {
        e.preventDefault();
        document.querySelector(".clickedFromPreferButton--form").submit();
      });
    });
  }
};

const dataAttPreferCart = () => {
  const preferLaber = document.querySelector("td.prefer-price");
  if (preferLaber) {
    preferLaber.setAttribute("data-title", "Cena Preferencyjna");
  }
};

const selectAll = () => {
  const labelSelect = document.getElementById("zaznacz_wszystko");

  if (labelSelect) {
    const parent = labelSelect.parentElement;
    parent.classList.add("zaznacz-wszystko");
  }
};

let checkboxRodoRegister;
jQuery(document).ready(function() {
  checkboxRodoRegister = document.querySelectorAll(
    '.woocommerce-form.woocommerce-form-register.register input[type="checkbox"]'
  );
});

function saveValue(e) {
  var id = e.id;
  var val = e.value;

  if (e.type === "radio") {
    localStorage.setItem("nowe_konto", "");
    localStorage.setItem("mam_konto", "");
    localStorage.setItem(id, val);
  } else if (e.type === "checkbox") {
    checkboxRodoRegister.forEach(checkbox => {
      if (checkbox.checked) {
        localStorage.setItem(checkbox.id, 1);
      } else {
        localStorage.setItem(checkbox.id, "");
      }
    });
  } else {
    localStorage.setItem(id, val);
  }
}

function saveAllValue() {
  checkboxRodoRegister.forEach(checkbox => {
    localStorage.setItem(checkbox.id, 1);
  });
}

function getSavedValue(v) {
  if (!localStorage.getItem(v)) {
    return "";
  }
  return localStorage.getItem(v);
}

const saveInputsBeforeReload = () => {
  const ids = [
    "reg_email",
    "billing_first_name",
    "billing_last_name",
    "billing_company",
    "billing_address_1",
    "billing_address_2",
    "billing_city",
    "billing_postcode",
    "billing_phone",
    "data_urodzenia",
    "numer_karty_cashback"
  ];

  ids.forEach(id => {
    const el = document.getElementById(id);
    if (el) {
      el.addEventListener("keyup", e => saveValue(e.target));
      el.value = getSavedValue(id);
    }
  });
};

const saveCheckboxes = () => {
  let arr = ["nowe_konto", "mam_konto", "zgoda_regulamin"];
  const ele = document.querySelector(".wrapp-rodo");
  if (ele) {
    const inputs = ele.querySelectorAll("input");
    inputs.forEach(input => arr.push(input.id));

    if (arr.length > 0) {
      arr.forEach(id => {
        const el = document.getElementById(id);
        el.addEventListener("change", e => {
          saveValue(e.target);
          getSavedValue(e.target);
        });
        el.checked = getSavedValue(id);
      });
    }
  }
};

const selectAllSave = () => {
  const labelSelect = document.getElementById("zaznacz_wszystko");
  if (labelSelect) {
    labelSelect.addEventListener("click", saveAllValue);
  }
};

const showFilterFn = () => {
  jQuery("#showFilters").on("click", function(e) {
    jQuery(".filters-wrapper").toggleClass("d-none");
    e.preventDefault();
  });
};

const clearFormAfterSuccesRegister = () => {
  for (var key in localStorage) {
    if (
      !key.includes("wc_") &&
      !key.includes("_hjid") &&
      !key.includes("hash") &&
      !key.includes("key")
    ) {
      localStorage.setItem(key, "");
    }
  }
  checkboxRodoRegister.forEach(checkbox => {
    localStorage.setItem(checkbox.id, "");
  });
};


const clickBasktet = () => {
  if(window.innerWidth < 1000){
    const basket = document.querySelector('#koszyk')
    const dropDown = document.querySelector('.dropdown-menu.dropdown-menu-mini-cart')
    basket.addEventListener('click',(e)=>{
      e.preventDefault();
      dropDown.style.display = 'none';
      window.location.href = basketURL;
    })
  }
  
}

jQuery(document).ready(function() {
  loginRegisterSwitch();
  dataAttPreferCart();
  selectAll();
  switchTabsCookie();
  saveInputsBeforeReload();
  saveCheckboxes();
  backToOrderFromRegister();
  showFilterFn();
  selectAllSave();
  clickBasktet()
});
