<div class="ingredients">
    <p class="heading-caveat">Składniki</p>
    <?php if ( have_rows('skladniki_produktu') ) : ?>

    <?php while( have_rows('skladniki_produktu') ) : the_row(); ?>

    <div class="tekst-skladniki"> <?php the_sub_field('tekst'); ?></div>

    <ul class="skladniki-list">
        <?php if ( have_rows('skladniki') ) : ?>

        <?php while( have_rows('skladniki') ) : the_row(); ?>

        <li><?php the_sub_field('skladnik'); ?></li>

        <?php endwhile; ?>

        <?php endif; ?>

    </ul>

    <?php endwhile; ?>

    <?php endif; ?>

</div>