<?php if (is_checkout()) { ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
var phones = [{
    "mask": "(+48) ### ### ###",
    "placeholder": "(+48) ___ ___ ___"
}];
jQuery('#billing_phone').inputmask({
    mask: phones,
    greedy: false,
    definitions: {
        '#': {
            validator: "[0-9]",
            cardinality: 1
        }
    }
});

var postCode = [{
    "mask": "##-###",
    "placeholder": "__-___"
}];

const postCodeInputs = ['#billing_postcode', '#_billing_company_postcode', '#shipping_postcode']
postCodeInputs.forEach(input => {
    jQuery(input).inputmask({
        mask: postCode,
        greedy: false,
        definitions: {
            '#': {
                validator: "[0-9]",
                cardinality: 1
            }
        }
    });
})


const textInput = ['#billing_first_name', '#billing_last_name', "#billing_city", "#_billing_company_city",
    '#shipping_first_name', '#shipping_last_name', '#shipping_city'
]
textInput.forEach(input => {
    const text = jQuery(input).parent().parent().text();
    // jQuery(input).attr('title', `Wpisz ${text}`)



    jQuery(input).on("keypress", function(e) {
        var value = String.fromCharCode(e.which);
        var pattern = new RegExp(/[AaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpRrSsŚśTtUuWwYyZzŹźŻż]/i);
        return pattern.test(value);
    });
})



var nipMask = [{
    "mask": "### ### ####",
    "placeholder": "___ ___ ____"
}];
jQuery('#_billing_nip').inputmask({
    mask: nipMask,
    greedy: false,
    definitions: {
        '#': {
            validator: "[0-9]",
            cardinality: 1
        }
    }
});

textInput.forEach(input => {
    jQuery('body').on('blur change', input, function() {
        var wrapper = jQuery(this).closest('.form-row');
        if (/\d/.test(jQuery(this).val())) {
            wrapper.addClass('woocommerce-invalid')
        } else {
            wrapper.addClass('woocommerce-validated');
        }
    });
})

jQuery('body').on('blur change', '#_billing_company_city', function() {
    const wrapper = jQuery(this);
    const label = jQuery('label[for="_billing_company_city"]')
    if (jQuery(this).val().length < 3) {
        wrapper.addClass('woocommerce-invalid');
        label.addClass('woocommerce-invalid');
    } else {
        wrapper.addClass('woocommerce-validated');
        label.addClass('woocommerce-validated');
    }
});

jQuery('body').on('blur change', '#billing_city', function() {
    const wrapper = jQuery(this);
    const label = jQuery('label[for="billing_city"]')
    if (jQuery(this).val().length < 3) {
        wrapper.addClass('woocommerce-invalid');
        label.addClass('woocommerce-invalid');
    } else {
        wrapper.addClass('woocommerce-validated');
        label.addClass('woocommerce-validated');
    }
});

jQuery('body').on('blur change', '#billing_phone', function() {
    const wrapper = jQuery(this);
    const label = jQuery('label[for="billing_phone"]')

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    let filterValue = replaceAll(jQuery(this).val(), /[^A-Za-z0-9\-+]/, '');

    if (filterValue.length < 12) {
        wrapper.removeClass('woocommerce-validated');
        label.removeClass('woocommerce-validated');
        wrapper.addClass('woocommerce-invalid');
        label.addClass('woocommerce-invalid');
    } else {
        wrapper.addClass('woocommerce-validated');
        label.addClass('woocommerce-validated');
        wrapper.removeClass('woocommerce-invalid');
        label.removeClass('woocommerce-invalid');
    }
});


const emptyField = ['_billing_company', '_billing_nip', '_billing_company_postcode', '_billing_company_street']
emptyField.forEach(field => jQuery('body').on('blur change', `#${field}`, function() {
    const wrapper = jQuery(this);
    const label = jQuery(`label[for="${field}"]`)
    if (jQuery(this).val().length < 2) {
        wrapper.addClass('woocommerce-invalid');
        label.addClass('woocommerce-invalid');
    } else {
        wrapper.addClass('woocommerce-validated');
        label.addClass('woocommerce-validated');
    }
}))
</script>

<?php }; ?>