<div class="container m-auto text-center">
    <p class="heading-roboto m-auto">Nasze produkty</p>
    <div class="d-flex justify-content-between m-auto col-12 flex-wrap">
        <?php

$args ='';
if(get_field('wyroznione_produkty_top', 'option')) {

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 3,
        'post__in'=> get_field('wyroznione_produkty_top', 'option')
      );
} else {

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 3,
        'meta_key' => 'total_sales',
        'orderby' => 'meta_value_num',
      );
}


  $loop = new WP_Query( $args );
  if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    wc_get_template_part( 'content', 'productOnFront' );
    endwhile;
  } else {
    echo __( 'No products found' );
  }
  wp_reset_postdata();
  ?>
    </div>
</div>