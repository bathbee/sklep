<?php

/**
 * Template Name: WPIS
 *
 * @package WordPress
 * 
 * 
 */
get_header(); ?>
<div>
    <main role="main">
        <!-- section -->

        <div style="background:url(<?php echo get_template_directory_uri() ?>/img/bg-bathbee.png)"
            class="container-fluid bread-header-archive">
            <h1 class="d-block col-12 text-center"><?php the_title(); ?></h1>
        </div>
        <section class="container" style="min-height:60vh;margin-top:30px">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" class="wrapper-post">




                <?php
                                global $post;
                                $content = apply_filters('the_content', $post->post_content);
                                echo $content
                                ?>


            </article>
            <!-- /article -->

            <?php endwhile; ?>

            <?php else : ?>

            <!-- article -->
            <article>

                <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

            </article>
            <!-- /article -->

            <?php endif; ?>

        </section>
        <!-- /section -->
    </main>
</div>


<?php get_footer(); ?>