<?php get_header(); ?>

<main role="main" class="error-page-wrapper">

    <article id="post-404" class="error-page">
        <img src="<?php echo get_template_directory_uri(); ?>/img/sad-error.png" alt="error">
        <h1>Przepraszamy, ale ta strona nie istnieje.</h1>

        <a class="btn-standard" href="<?php echo home_url(); ?>">Wróć na stronę główną</a>


    </article>

</main>

<?php get_footer(); ?>