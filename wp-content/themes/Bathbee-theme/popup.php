<?php if(get_field('wlacz_popup', 'option') && is_front_page()) : 
    
    $product = get_field('wybierz_produkt','option');
  
    $img;
    if( get_field('dodac_wlasny_obrazek_produktu','option')){
        $img = get_field('wlasny_obrazek_produktu','option');
    } else {

        if(get_field('dodatkowy_obrazek_produktu',$product->ID )) {
            $img = get_field('dodatkowy_obrazek_produktu',$product->ID );
        } else {
            $img = wp_get_attachment_url( wc_get_product($product->ID)->get_image_id() );
        }
       
    }
   
    
    $title = get_field('tytul_popupa','option');
    $subtitle = get_field('subtytul_popupa','option');
    $desc = get_field('opis_popupa','option');
    $time = get_field('po_ilu_sekundach_ma_sie_wyswietlic','option');
    $home = get_home_url();
    $link= "$home/?add-to-cart=$product->ID";

    $normalPrice = number_format(wc_get_product($product->ID)->get_price(), 2);
    $prefPrice = calculateDiscount($product->ID);
    
    ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/popup-assets/css/lunar.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/popup-assets/css/animate.min.css" />

<div class="modal fade modal-popup" style="z-index:9999999;" id="popupModal" tabindex="-1" role="dialog"
    aria-labelledby="popupModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered  modal-lg  col-12 mx-auto" role="document">
        <div class="modal-content">
            <div class="container-fluid">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-5 bg-img img-wrapper-popup p-0">
                        <img src="<?php echo $img;?>" alt="">
                    </div>
                    <div class="col-md-7 py-5 px-sm-5 my-auto wrapp-content-text-popup">
                        <div class="title-popup"><?php echo $title;?></div>
                        <div class="subtitle-popup"><?php echo $subtitle;?></div>
                        <div class="buttons-popup">

                            <?php if($prefPrice): ?>

                            <p class="normal-price-popup"><?php echo $normalPrice;?> zł</p>
                            <a class="pref-price-popup" href="<?php echo  $link;?>"><i class="fas fa-shopping-cart"></i>
                                <?php echo $prefPrice;?> zł</a>
                            <?php else: ?>
                            <a class="pref-price-popup only-normal-price" href="<?php echo  $link;?>"><i
                                    class="fas fa-shopping-cart"></i> <?php echo $normalPrice;?> zł</a>
                            <?php endif;?>

                        </div>
                        <div class="desc-popup"> <?php echo $desc;?></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
setTimeout(function() {
    $(".modal:not(.auto-off)").modal("show");
}, <?php echo $time*1000;?>);
</script>
<?php endif;?>