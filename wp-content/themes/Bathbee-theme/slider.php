<?php

function scripts_custom()
{
    wp_enqueue_script('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), '', false);
}
add_action('wp_enqueue_scripts', 'scripts_custom');

if (function_exists('acf_add_options_page')) {

    acf_add_options_page('Slider');
}

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5d6911a1cc789',
        'title' => 'Slider',
        'fields' => array(
            array(
                'key' => 'field_5d6911a24a0ae',
                'label' => 'Dodaj Slide',
                'name' => 'dodaj_slide',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'block',
                'button_label' => 'Dodaj kolejny slide',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5d6911af4a0af',
                        'label' => 'Tło',
                        'name' => 'tlo',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'url',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),

                    array(
                        'key' => 'field_5d6911e24a0b2',
                        'label' => 'Nagłówek',
                        'name' => 'naglowek',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5d6911e24a0b3',
                        'label' => 'Link',
                        'name' => 'link',
                        'type' => 'link',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),



                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-slider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;







function slider()
{ ?>


<div class="slider">
    <?php if (have_rows('dodaj_slide', 'option')) : ?>

    <?php while (have_rows('dodaj_slide', 'option')) : the_row(); ?>


    <a href="<?php echo get_sub_field('link')['url']; ?>">
        <div class="slide" style="background-image:url(<?php the_sub_field('tlo'); ?>);">
            <h2 style="opacity:0;width:0;height:0;position:absolute;top:0;left:0"><?php the_sub_field('naglowek'); ?>
            </h2>
        </div>
    </a>



    <?php endwhile; ?>

    <?php endif; ?>


</div>
<style>
.slider .slide {
    display: flex !important;
    min-height: 530px;
    background-position: top center;
    background-size: contain;
    background-repeat: no-repeat;
    width: 100%;
}

.slider .slide:focus {
    outline: none
}



.slick-prev:hover,
.slick-next:hover,
.slick-prev:focus,
.slick-next:focus {
    color: transparent;
}

.slick-prev,
.slick-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    top: 50%;
    display: block;
    width: 20px;
    height: 20px;
    padding: 0;
    -webkit-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
    cursor: pointer;
    color: transparent;
    border: none;
    outline: none;
    background: transparent;
    color: transparent;
    /* visibility: hidden */
}

.slick-prev:before {
    visibility: visible;
    content: '\f053';
    font-size: 30px;
    font-weight: 900;
    font-family: "Font Awesome 5 Free";
    -webkit-font-smoothing: antialiased;
    display: inline-block;
    font-style: normal;
    font-variant: normal;
    text-rendering: auto;
    line-height: 1;
    color: white;
}

.slick-next:before {
    visibility: visible;
    content: '\f054';
    font-size: 30px;
    font-weight: 900;
    font-family: "Font Awesome 5 Free";
    -webkit-font-smoothing: antialiased;
    display: inline-block;
    font-style: normal;
    font-variant: normal;
    text-rendering: auto;
    line-height: 1;
    color: white;
}

.slick-next {
    right: 10px !important;
    z-index: 99;
}

.slick-prev {
    left: 10px !important;
    z-index: 99;
}

.slide--wrapper-content {
    margin: 30px 0;
}


[type=button]:focus,
[type=button]:hover,
[type=submit]:focus,
[type=submit]:hover,
button:focus,
button:hover {
    color: transparent;
    background: transparent;
    outline: none
}

@media(max-width:500px) {

    .slick-prev,
    .slick-next {
        font-size: 13px;
        top: 40% !important
    }

    .slick-prev:before,
    .slick-next:before {
        font-size: 13px !important;
    }

    .slick-prev {
        left: 3px !important;
        z-index: 99;
    }

    .slick-next {
        right: 3px !important;
        z-index: 99;
    }
}

@media(max-width:1000px) {
    .slider .slide {
        background-position: 49%;
        background-size: cover;
        background-repeat: no-repeat;
        min-height: 130px;
    }

}
</style>


<script>
jQuery(document).ready(() => {

    jQuery('.slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        dots: true,
        arrows: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                dots: true,
                arrows: true
            }
        }]
    });
})
</script>


<?php }

add_shortcode('slider', 'slider');

?>