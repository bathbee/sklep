## [1.0.4] - 2019-08-05
### Changed
- Fixed Fatal error: boolean given in mutex, #PB-1241

## [1.0.3] - 2019-07-08
### Changed
- Added close counter to rate notice

## [1.0.2] - 2019-07-03
### Changed
- Added rate notice with variants

## [1.0.1] - 2019-06-26
### Changed
- Moved html-order-add_shipping-metabox.php here

## [1.0.0] - 2019-06-17
### Added
- Init